export const theme = {
	colors: {
		primary: '#93c21a',
		primaryAccent: '#658b00'
	},
	boxShadow: '0 15px 35px rgba(0,0,0,0.15)'
}